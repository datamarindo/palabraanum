
# palabraanum

Función auxiliar para buscar cifras textuales y reemplazarlas por cifras numéricas en español; enfocada a formularios no estructurados con presencia de respuestas de dispositivos móviles, en los cuales se facilita escribir "diez" directamente en el teclado alfabético, en lugar de cambiar a teclado numérico.

Una vez convertido, se pueden extraer las cifras numéricas con _expresiones regulares_, tales como `\\d+` o
`[[:digit]]+`

## Instalación

``` r
devtools::install_gitlab("datamarindo/palabraanum")
```

## Ejemplo


``` r
library(palabraanum)
cc = c("en los años mil seiscientos", "diecisiete años", "cinco gatitos muy bañaditos")
palabraanum(cc)

# [1] "en los años 1600"        "17 años"                 "5 gatitos muy bañaditos"
```

